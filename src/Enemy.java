import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrew Meads on 9/01/2018.
 */
public class Enemy {

    private static final int SPRITE_WIDTH = 32, SPRITE_HEIGHT = 32;

    private int x, y, width, height, speed;

    private Direction direction;

    private Image image;


    public Enemy(int x, int y, int speed) {

//        direction = Direction.Up;

        try {
            this.image = ImageIO.read(new File("enemy_3.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.x = x;
        this.y = y;
        this.width = SPRITE_WIDTH;
        this.height = SPRITE_HEIGHT;
        this.speed = speed;

    }

    public void move(Direction direction, int windowWidth, int windowHeight) {

        // TODO Step 6.

        if (direction == Direction.Left){
            x -= speed;
            if (x  - SPRITE_WIDTH < 0) {
                x = 0;
                this.direction = Direction.Up;
                return;
            }
            this.direction = Direction.Left;
            return;
        }

        if (direction == Direction.Right){
            x += speed;
            if ((x + SPRITE_WIDTH) > windowWidth) {
                x = windowWidth - SPRITE_WIDTH;
                this.direction = Direction.Up;
                return;
            }
            this.direction = Direction.Right;
            return;
        }

        if (direction == Direction.Up){
            y -= speed;
            if ((y - SPRITE_HEIGHT) < 0)
                y = 0;
            this.direction = Direction.Up;
            return;
        }

        if (direction == Direction.Down){
            y += speed;
            if ((y + SPRITE_HEIGHT) > windowHeight) {
                y = windowHeight - SPRITE_HEIGHT;
                this.direction = Direction.Up;
                return;
            }
            this.direction = Direction.Down;
            return;
        }


    }

    public void paint(Graphics g) {

        // TODO Step 7 (modify this).



        if (this.direction == Direction.Up){
            drawShip(g, 0, 0);
            return;
        }

        if (this.direction.equals(Direction.Down)){
            drawShip(g, 0, 0);
            return;
        }

        if (this.direction == Direction.Left){
            drawShip(g, 0, 0);
            return;
        }

        if (this.direction == Direction.Right){
            drawShip(g, 0, 0);
            return;
        }



    }

    private void drawShip(Graphics g, int sx1, int sy1) {
        g.drawImage(image, x, y, x + width, y + height, sx1, sy1, width+sx1, height+sy1, null);
    }

}
